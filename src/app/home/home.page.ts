import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  data: any;
  activeQuestion: any;
  questionCounter: number;
  isCorrect: boolean;
  optionCounter: number;
  totalQuestions: number;
  showQuestion: boolean;
  activeOption: any;
  answers: any[] = [];
  showResult: boolean;
  firstPage: boolean;
  correctAnswer: any;
  totalCorrect: number;
  percentage: number;
  feedback: string;
  lastPage: boolean;
  correctAnswerArray: any[] = [];
  time: any;
  running: boolean;
  times: any;
  resultTime: any;
  activeImage: any;

  constructor() {}

  ngOnInit() {
    fetch('./assets/data/data.json').then(res => res.json())
    .then(json => {
      this.data = json;
    
    });
    this.questionCounter = 0;
    this.showQuestion = false;
    this.firstPage = true;
    this.answers = [];
    this.lastPage = false;
    this.running = false;
    
  }
  startQuiz() {
    this.showQuestion = true;
    this.firstPage = false;
    this.setQuestion();
    this.start();
  }

  startOver() {
    this.firstPage = true;
    this.lastPage = false;
    this.answers = [];
    this.questionCounter = 0;
    this.showQuestion = false;
    this.correctAnswerArray = [];
    this.time = 0;
    location.reload(true);
  }

  setQuestion() {
    if (this.questionCounter === this.data.quizes.length) {
      this.questionCounter = 0;
 
    }

    this.optionCounter = 0;
    this.totalQuestions = this.data.quizes.length;
    this.isCorrect = false;
    this.activeImage = this.data.quizes[this.questionCounter].photo;
    this.activeQuestion = this.data.quizes[this.questionCounter].question;
    this.activeOption = this.data.quizes[this.questionCounter].options;
    this.correctAnswer = this.data.quizes[this.questionCounter].correctAnswer;
    this.questionCounter++;
    
  }

  checkQuestion(questionA: number, cAnswer: number) {
    if (this.questionCounter === 4) {
      this.stop();
      if (questionA === cAnswer) {
        this.answers.push(questionA);
      }
      this.showQuestion = false;
      this.showResult = true;
      this.totalCorrect = this.answers.length;
      this.percentage = (this.totalCorrect/this.totalQuestions) * 100;
      
      if (this.answers.length === 0) {
        this.feedback = "Not very well but you can try again!";
      }
      if (this.answers.length === 1) {
        this.feedback = "Not a pro but very nice try!";
      }
      if (this.answers.length === 2) {
        this.feedback = "Well done!";
      } 
      if (this.answers.length === 3) {
        this.feedback = "You know almost all! Maybe you have couple furry friends?";
      }
      if (this.answers.length === 4) {
        this.feedback = "Bravo! There must be pawprints in you´re heart!";
      } 
    }
    else {
      if (questionA === cAnswer) {
        this.answers.push(questionA);
      }
      
      this.setQuestion();
    }

  }

  correctAnswers() {
      for (let i = 0; i < this.data.quizes.length; i++) {
        for (let x = 0; x < this.data.quizes[i].options.length; x++) {
          
          if (this.data.quizes[i].correctAnswer === x) {
            this.correctAnswerArray.push(this.data.quizes[i].options[x]);
          }
          
        }
        
      }
      this.showResult = false;
      this.lastPage = true;
  }
  start() {
    this.time = performance.now();
    if (!this.running) {
        this.running = true;
    }
  }
  stop() {
    this.resultTime = this.time / 1000;
    this.resultTime = this.resultTime.toPrecision(2);
    this.running = false;
    this.time = 0;
  }
 
}
